import static org.junit.Assert.*;

import main.java.StringCalculator;
import org.junit.Test;

public class StringCalculatorTest {

    @Test
    public void testing() {

        StringCalculator SC = new StringCalculator();

        assertEquals(0, SC.add(""));
        assertEquals(7, SC.add("7"));
        assertEquals(357, SC.add("24,333"));
        assertEquals(10, SC.add("5,10,20,-25"));
        assertEquals(310, SC.add("5,10,20,-25,100,200"));
        assertEquals(5, "2\n3");
        //assertEquals(23, "5\n15,3");

    }

}
