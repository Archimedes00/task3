package main.java;

public class StringCalculator {

    public int add(String numbers) {
        if (isEmpty(numbers)) {
            return 0;
        }

        else if (!containsMoreThanOneNumbers(numbers)) {
            return Integer.parseInt(numbers);
        }
        else if (containsMoreThanOneNumbers(numbers)) {
            return extractNumbersAndAdd(numbers);
        }
        return 0;
    }

    private int extractNumbersAndAdd(String numbers) {
        int sum = 0;
        String[] values = numbers.split("[,\n]");
        for (String s: values) {
            sum += Integer.parseInt(s);
        }
        return sum;
    }

    private boolean containsMoreThanOneNumbers(String numbers) {
        return numbers.contains(",");
    }

    private boolean isEmpty(String numbers) {
        return numbers.isEmpty();
    }
}
